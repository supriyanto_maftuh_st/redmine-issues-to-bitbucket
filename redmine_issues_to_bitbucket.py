#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013 The American Society for Clinical Investigation
#
# Small script that retrieves issues from Redmine's REST API and generates zip
# files that can be imported into Bitbucket's issue tracker using the issue importer.
# Depends on pyredminews: https://github.com/ianepperson/pyredminews
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.


# EDIT THESE VARIABLES: 
# -----------------------------------------------------------------------------
# Map Redmine usernames to bitbucket usernames
userMap = {
   'redmine_user': 'bitbucket_user',
   None: None, # this is for the "Anonymous" Redmine user
}
# Map Redmine projects to repo slugs
projectToRepoMap = {
    'Big Project': 'bigproject',
    'Another Project': 'another',
}
# Map tracker names in Redmine to Bitbucket bug types (i.e. kinds)
# Redmine trackers can be found at <redmineUrl>/trackers
# Possible Bitbucket kinds:
# "bug"
# "enhancement"
# "proposal"
# "task"
trackerToKindMap = { 
    'Design': 'task',
    'Bug': 'bug',
    'Feature': 'enhancement',
    'Vendor': 'task',
    'Data': 'task',
    'Support': 'task',
}
# Map statuses from Redmine to Bitbucket
# Redmine statuses can be found at <redmineUrl>/issue_statuses
# Possible BitBucket statuses:
# "new"
# "open"
# "resolved"
# "on hold"
# "invalid"
# "duplicate"
# "wontfix"
statusMap = {
    'Resolved': 'resolved',
    'Closed': 'resolved',
    'New': 'new',
    'Assigned': 'open',
    'Feedback': 'on hold',
    'On Hold': 'on hold',
    'Rejected': 'invalid',
}
# Map priorities from Redmine to Bitbucket.
# Shouldn't need to change this since Redmine priorites are immutable.
# Possible BitBucket priorities:
# "trivial"
# "minor"
# "major"
# "critical"
# "blocker"
priorityMap = {
    'Low': 'trivial',
    'Normal': 'minor',
    'High': 'major',
    'Urgent': 'critical',
    'Immediate': 'blocker',
}
# Directory to output the ZIP files (will be created if it doesn't exist)
outputDir = 'issues'
# Directory containing ZIP exports for existing issues (will be ignored if it doesn't exist)
inputDir = 'issues_old'
# Version of Redmine
redmineVersion = 2.3
# -----------------------------------------------------------------------------

import os
import sys
import json
import zipfile
import redmine

class RedmineIssueFeed(object):
    # Represents a collection of Redmine issues
    def __init__(self):
        self.DB_FILENAME = "db-1.0.json"
        # self.dbs maps repo slugs to dictionaries.
        # Each dictionary will be serialized into a db-1.0.json file that'll
        # be used to generate the ZIP file for the associated repo.
        self.dbs = {}

    def addIssue(self, redmineIssue):
        # Takes in an Isssue object and sets appropritate fields in self.dbs
        print("Processing redmine issue #%i" % redmineIssue.id)

        repo = projectToRepoMap.get(redmineIssue.project.name, None)
        if repo is None:
            print(" - Failed to find repo for %s, skipping issue" % redmineIssue.project.name)
            return

        if repo not in self.dbs:
            self.loadIssuesForRepo(repo)

        author_username = redmineIssue.author.login
        if not userMap.has_key(author_username):
            print(" - Couldn't find username \"%s\" in userMap, skipping issue" % author_username)
            return

        status_name = redmineIssue.status.name
        if not statusMap.has_key(status_name):
            print(" - Couldn't find status \"%s\" in statusMap, skipping issue" % status_name)
            return

        tracker_name = redmineIssue.tracker.name
        if not trackerToKindMap.has_key(tracker_name):
            print(" - Couldn't find tracker \"%s\" in trackerToKindMap, skipping issue" % tracker_name)
            return

        issue = {
            'id': redmineIssue.id,
            'created_on': redmineIssue.created_on.isoformat(),
            'updated_on': redmineIssue.updated_on.isoformat(),
            'kind': trackerToKindMap[tracker_name],
            'status': statusMap[status_name],
            'title': redmineIssue.subject,
            'priority': priorityMap[redmineIssue.priority.name],
            'reporter': userMap[author_username],
            'content': redmineIssue.description,
            'milestone': None,
        }

        try:
            version = { "name": redmineIssue.fixed_version.name }
            if not version in self.dbs[repo]['versions']:
                self.dbs[repo]['versions'].append(version)
            issue['version'] = version['name']
        except AttributeError:
            issue['version'] = None

        try:
            component = { "name": redmineIssue.category.name }
            if not component in self.dbs[repo]['components']:
                self.dbs[repo]['components'].append(component)
            issue['component'] = component['name']
        except AttributeError:
            issue['component'] = None

        try:
            assignee_username = redmineIssue.assigned_to.login
            if not userMap.has_key(assignee_username):
                print(" - Couldn't find username \"%s\" in userMap, skipping issue" % assignee_username)
                return
            issue['assignee'] = userMap[assignee_username]
        except AttributeError:
            issue['assignee'] = None

        additional_content = self.getAdditionalContent(redmineIssue)
        if additional_content != "":
            issue['content'] += "\n" + additional_content

        # de-dupe by looping over all existing issues and comparing the 'id' field
        # Very inefficient, but the performance hit isn't that bad, and this is meant
        # to be a one-off script anyway.
        for i, existing_issue in enumerate(self.dbs[repo]['issues']):
            if existing_issue['id'] == issue['id']:
                self.dbs[repo]['issues'][i] = issue
                break
        else:
            self.dbs[repo]['issues'].append(issue)
        
        for journal in redmineIssue.journals:
            comment = self.getComment(journal, redmineIssue)
            if comment:
                # de-dupe in the same way as above
                for i, existing_comment in enumerate(self.dbs[repo]['comments']):
                    if existing_comment['id'] == comment['id']:
                        self.dbs[repo]['comments'][i] = comment
                        break
                else:
                    self.dbs[repo]['comments'].append(comment)
        
        return issue

    def loadIssuesForRepo(self, repo):
        print(" - Attempting to load exported Bitbucket issues for repo \"%s\"" % repo),
        try:
            archive = zipfile.ZipFile("%s/%s-issues.zip" % (inputDir, repo), "r")
            self.dbs[repo] = json.loads(archive.read(self.DB_FILENAME))
            print("...loaded %s issues" % len(self.dbs[repo]['issues']))
        except (KeyError, IOError):
            print("...none found")
            self.dbs[repo] = {
                'issues': [],
                'comments': [],
                'components': [],
                'milestones': [],
                'versions': [],
                'attachments': [],
                'logs': [],
                'meta': {
                    'default_assignee': None,
                    'default_component': None,
                    'default_kind': 'bug',
                    'default_milestone': None,
                    'default_version': None,
                }
            }

    def getComment(self, journal, redmineIssue):
        if not journal.notes: return
        username = journal.user.login
        if not userMap.has_key(username):
            print(" - Couldn't find username \"%s\" in userMap, skipping comment" % username)
            return
        return {
            "id": journal.id,
            "issue": redmineIssue.id,
            "content": journal.notes,
            "created_on": journal.created_on.isoformat(),
            "user": userMap[username],
            "updated_on": None,
        }

    def getAdditionalContent(self, redmineIssue):
        # Bitbucket doesn't support these fields yet, so append them to issue description
        # as an extra line so they don't get lost
        additional_fields = {
            'Start date': lambda i: i.start_date.strftime('%Y-%m-%d'),
            'Due date': lambda i: i.due_date.strftime('%Y-%m-%d'),
            'Spent hours': lambda i: i.spent_hours,
            'Estimated hours': lambda i: i.estimated_hours,
            '% Done': lambda i: str(i.done_ratio) + "%",
        }
        additional_content = ""
        for (label, value_fetch) in additional_fields.items():
            try:
                value = value_fetch(redmineIssue)
                additional_content += "\n%s: %s\n" % (label, value)
            except AttributeError: pass
        return additional_content
            
    def saveIssues(self):
        for repo in self.dbs:
            self.saveIssuesForRepo(repo)

    def saveIssuesForRepo(self, repo):
        print("Saving issues for repo %s" % repo)

        archive = zipfile.ZipFile("%s/%s-issues.zip" % (outputDir, repo), "w")
        db = json.dumps(self.dbs[repo], indent=3)
        archive.writestr(self.DB_FILENAME, db)
        archive.close()

if __name__ == '__main__':   
    if len(sys.argv) != 3:
        usage = "Usage: {0} <redmineUrl> <redmineApiKey>\nExample: {0} http://redmine.jci.org f00ba5" 
        print(usage.format(sys.argv[0]))
        sys.exit(1)
    redmineUrl = sys.argv[1]
    redmineApiKey = sys.argv[2]

    try:
        os.mkdir(outputDir)
    except OSError: pass

    feed = RedmineIssueFeed()
    redmine = redmine.Redmine(redmineUrl, key=redmineApiKey, debug=False, version=redmineVersion)

    # populate the user cache (seems to be needed to be able to access issue.author.login)
    list(redmine.users)

    for issue in redmine.issues(status_id="closed", limit=100):
        feed.addIssue(issue)

    for issue in redmine.issues(status_id="open", limit=100):
        feed.addIssue(issue)

    feed.saveIssues()
